import React from 'react'
import {
    Router,
    Switch,
    Route
  } from "react-router-dom";

import Home from './Home';  
import history from './History';

function Routes(){
    return(
        <Router history={history}>
            <Switch>
                <Route exact path="/" component={Home} />             
            </Switch>
        </Router>
    )
}
export default Routes;
