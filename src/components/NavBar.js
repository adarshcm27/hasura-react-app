import React from  'react';
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Typograpy from '@material-ui/core/Typography'
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme)=>({
    title: {
         flexGrow: 1,
    } 
 }));


function NavBar(){
    const classes = useStyles();
    
    return (
        <div>
            <AppBar position="static">
                <Toolbar>
                    <Typograpy  variant="h6" color="inherit" align="left" className={classes.title}>
                        Hasura Web Application
                    </Typograpy>
                    <Typograpy  color="inherit">
                        
                    </Typograpy>
                </Toolbar>
            </AppBar>
        </div>
    ) 
}
export default NavBar;