import React,{useEffect, useState}  from 'react';
import NavBar from './components/NavBar';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableFooter from '@material-ui/core/TableFooter';
import TablePagination from '@material-ui/core/TablePagination';
import Paper from '@material-ui/core/Paper';
import axios from 'axios';
import './App.css';

const StyledTableCell = withStyles((theme) => ({
    head: {
      backgroundColor: theme.palette.common.black,
      color: theme.palette.common.white,
    },
    body: {
      fontSize: 14,
    },
  }))(TableCell);
  
  const StyledTableRow = withStyles((theme) => ({
    root: {
      '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.action.hover,
      },
    },
  }))(TableRow);

const useStyles = makeStyles({
    root: {
        width: '100%',
      },
      table: {
        minWidth: 500,
      },
});
  
function Home(){

    const [customers,setCustomers] = useState([]);
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(5);
    const [totalCount, setTotalCount] = useState(0);

    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(+event.target.value);
        setPage(0);
    };

    useEffect(() => {
        
        const fetchData = async () => {
            const url = "http://18.219.89.109:8585/api";

            const result = await axios.get(url+'/customers',{
                params : {
                size:rowsPerPage,
                page:page
                },
            });
       
            setCustomers(result.data.customers);  
            setTotalCount(result.data.totalItems);  

        };
    
        fetchData();
      },[page, rowsPerPage]);

    const classes = useStyles();
    return(
        <div>
            <NavBar></NavBar>
            
            <TableContainer component={Paper} >
                <Table aria-label="customized table">
                    <TableHead>
                        <TableRow>
                            <StyledTableCell>First Name</StyledTableCell >
                            <StyledTableCell>Last Name</StyledTableCell >
                            <StyledTableCell>Email</StyledTableCell >
                            <StyledTableCell >State</StyledTableCell >
                            <StyledTableCell>Country</StyledTableCell >                   
                        </TableRow>
                    </TableHead>
                    <TableBody>
                    {customers.map((row) => (
                        <StyledTableRow  key={row.id} >
                            <StyledTableCell >{row.first_name}</StyledTableCell >                             
                            <StyledTableCell >{row.last_name}</StyledTableCell >
                            <StyledTableCell >{row.email}</StyledTableCell >
                            <StyledTableCell >{row.state === null ? 'N/A' : row.state}</StyledTableCell >
                            <StyledTableCell >{row.country}</StyledTableCell >
                        </StyledTableRow >
                    ))}
                    </TableBody>  
                </Table>
                </TableContainer>  
                
                <TablePagination
                    rowsPerPageOptions={[5, 10, 20]}
                    component="div"
                    count={totalCount}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    onChangePage={handleChangePage}
                    onChangeRowsPerPage={handleChangeRowsPerPage}
                />  
                
        </div>
    )
}
export default Home;
