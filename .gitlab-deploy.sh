set -f
string=$DEV_DEPLOY_SERVER
array=(${string//,/ })

for i in "${!array[@]}"; do
  echo "Deploying project on server ${array[i]}"
  rsync -rav --delete build/ ubuntu@${array[i]}:/home/ubuntu/hasura-web/
  echo "Deployed"
done